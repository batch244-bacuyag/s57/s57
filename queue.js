let collection = [];

function print() {
  let result = [];
  for (let i = 0; i < collection.length; i++) {
    result[i] = collection[i];
  }
  return result;
}

function enqueue(element) {
	collection[collection.length] = element;
	return collection;
}

function dequeue() {
	let newCollection = [];
	for (let i = 1; i < collection.length; i++) {
		newCollection[newCollection.length] = collection[i];
	}
	collection = newCollection;
	return collection;
}

function front() {
	return collection[0];
}

function size() {
	let count = 0;
	for (let i = 0; i < collection.length; i++) {
		count++;
	}
	return count;
}

function isEmpty() {
	return collection.length === 0;
}

module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};